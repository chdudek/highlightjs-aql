<span class="hljs-comment">// Valid numbers</span>
<span class="hljs-number">1</span>
<span class="hljs-operator">+</span><span class="hljs-number">1</span>
<span class="hljs-number">42</span>
<span class="hljs-operator">-</span><span class="hljs-number">1</span>
<span class="hljs-operator">-</span><span class="hljs-number">42</span>
<span class="hljs-number">1.23</span>
<span class="hljs-operator">-</span><span class="hljs-number">99.99</span>
<span class="hljs-number">0.5</span>
<span class="hljs-number">.5</span>
<span class="hljs-operator">-</span><span class="hljs-number">4.87e103</span>
<span class="hljs-operator">-</span><span class="hljs-number">4.87E103</span>

<span class="hljs-comment">// Invalid numbers</span>
1.
01.23
00.23
00