# highlightjs-aql

![version](https://badgen.net/npm/v/highlightjs-aql)

## Description

[ArangoDB Query Language (AQL)](https://docs.arangodb.com/3.11/aql/) grammar for highlight.js

## How to use in browser

```html
<script type="text/javascript" src="/path/to/highlight.min.js"></script>
<script
  type="text/javascript"
  charset="UTF-8"
  src="/path/to/highlightjs-aql/dist/aql.min.js"
></script>
<script type="text/javascript">
  hljs.highlightAll()
</script>
```

## License

MIT License

## Todo

- Add support for inline AQL in Javascript (using `query` and `aql` templating)
- Work on auto-detection
